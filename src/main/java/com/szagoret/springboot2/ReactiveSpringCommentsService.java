package com.szagoret.springboot2;

import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.filter.reactive.HiddenHttpMethodFilter;


@SpringCloudApplication
public class ReactiveSpringCommentsService {

    private static org.slf4j.Logger logger = LoggerFactory.getLogger(ReactiveSpringCommentsService.class);

    public static void main(String[] args) {

        SpringApplication.run(ReactiveSpringCommentsService.class, args);
    }


    /**
     * make the HTTP DELETE methods work properly
     */
    @Bean
    HiddenHttpMethodFilter hiddenHttpMethodFilter() {
        return new HiddenHttpMethodFilter();
    }

//    @Bean
//    CommandLineRunner setUp(MongoOperations operations) {
//        return args -> operations.dropCollection(Comment.class);
//    }
}
