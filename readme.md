**Spring Boot 2 Application** *Reactive design*

- WebFlux framework
- MongoDB (MLab database)
- Heroku  (Application deployment)
- Bitbucket (pipelines)




![alt text](https://docs.spring.io/spring/docs/5.0.0.BUILD-SNAPSHOT/spring-framework-reference/html/images/webflux-overview.png)